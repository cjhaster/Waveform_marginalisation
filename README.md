Implement NRHybSur3dq8 as a WF, through gwsurrogate, using both the "normal" and truncated surrogate bases.
The difference between these should then be a reasonable measure for the uncertainty in a WF given a point in parameter space.

Will use the `WF_marg_igwn-py37` environment on CIT for now. It's also enabled as a jupyterlab kernel for me.