From 1281c6a880c613fd76b35762e819222fff5c7e01 Mon Sep 17 00:00:00 2001
From: "carl-johan.haster" <carl-johan.haster@ligo.org>
Date: Thu, 24 May 2018 17:02:16 -0700
Subject: [PATCH] add WFmarg structure for Benasque checks VERSION8

---
 lalinference/src/LALInference.c           | 202 ++++++++++++++++++++++++++++++
 lalinference/src/LALInference.h           |  23 +++-
 lalinference/src/LALInferenceInit.h       |   2 +
 lalinference/src/LALInferenceInitCBC.c    |  93 ++++++++++++++
 lalinference/src/LALInferenceLikelihood.c | 131 ++++++++++++++++++-
 5 files changed, 449 insertions(+), 2 deletions(-)

diff --git a/lalinference/src/LALInference.c b/lalinference/src/LALInference.c
index 53bbabe537..b00a1341e1 100644
--- a/lalinference/src/LALInference.c
+++ b/lalinference/src/LALInference.c
@@ -4438,3 +4438,205 @@ void LALInferencePrintSplineCalibration(FILE *output, LALInferenceThreadState *t
         }
     }
 }
+
+int LALInferenceWFMargFactorROQ(REAL8Vector *logfreqs,
+          REAL8Vector *deltaAmpsPlus,
+          REAL8Vector *deltaPhasesPlus,
+          REAL8Vector *deltaAmpsCross,
+          REAL8Vector *deltaPhasesCross,
+          REAL8Sequence *freqNodesLin,
+          COMPLEX16Sequence **wfMargFactorROQLinPlus,
+          COMPLEX16Sequence **wfMargFactorROQLinCross,
+          REAL8Sequence *freqNodesQuad,
+          COMPLEX16Sequence **wfMargFactorROQQuadPlus,
+          COMPLEX16Sequence **wfMargFactorROQQuadCross){
+
+  // need Quad and Lin for both plus and cross bits, so four input and four output
+
+  gsl_interp_accel *pampAcc = NULL, *pphaseAcc = NULL, *campAcc = NULL, *cphaseAcc = NULL;
+  gsl_interp *pampInterp = NULL, *pphaseInterp = NULL, *campInterp = NULL, *cphaseInterp = NULL;
+
+  int status = XLAL_SUCCESS;
+  const char *fmt = "";
+
+  size_t N = 0;
+
+  /* should I check that calFactorROQ = NULL as well? */
+
+  if (logfreqs == NULL || deltaAmpsPlus == NULL || deltaPhasesPlus == NULL || deltaAmpsCross == NULL ||
+       deltaPhasesCross == NULL || freqNodesLin == NULL || freqNodesQuad == NULL) {
+    status = XLAL_EINVAL;
+    fmt = "bad input";
+    goto cleanup;
+  }
+
+  if (logfreqs->length != deltaAmpsPlus->length || deltaAmpsPlus->length != deltaPhasesPlus->length ||
+      logfreqs->length != deltaAmpsCross->length || deltaAmpsCross->length != deltaPhasesCross->length ||
+      freqNodesLin->length != (*wfMargFactorROQLinPlus)->length || freqNodesQuad->length != (*wfMargFactorROQQuadPlus)->length ||
+      freqNodesLin->length != (*wfMargFactorROQLinCross)->length || freqNodesQuad->length != (*wfMargFactorROQQuadCross)->length) {
+    status = XLAL_EINVAL;
+    fmt = "input lengths differ";
+    goto cleanup;
+  }
+
+  N = logfreqs->length;
+
+  pampInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+  pphaseInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+  campInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+  cphaseInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+
+  if (pampInterp == NULL || pphaseInterp == NULL || campInterp == NULL || cphaseInterp == NULL) {
+    status = XLAL_ENOMEM;
+    fmt = "could not allocate GSL interpolation objects";
+    goto cleanup;
+  }
+
+  pampAcc = gsl_interp_accel_alloc();
+  pphaseAcc = gsl_interp_accel_alloc();
+  campAcc = gsl_interp_accel_alloc();
+  cphaseAcc = gsl_interp_accel_alloc();
+
+  if (pampAcc == NULL || pphaseAcc == NULL || campAcc == NULL || cphaseAcc == NULL) {
+    status = XLAL_ENOMEM;
+    fmt = "could not allocate interpolation acceleration objects";
+    goto cleanup;
+  }
+
+  REAL8 lowf = exp(logfreqs->data[0]);
+  REAL8 highf = exp(logfreqs->data[N-1]);
+  REAL8 pdA = 0.0, pdPhi = 0.0, cdA = 0.0, cdPhi = 0.0;
+
+  for (unsigned int i = 0; i < freqNodesLin->length; i++) {
+    REAL8 f = freqNodesLin->data[i];
+    REAL8 logf = log(f);
+    if (f < lowf || f > highf) {
+      pdA = 0.0;
+      pdPhi = 0.0;
+      cdA = 0.0;
+      cdPhi = 0.0;
+    } else {
+      pdA = gsl_interp_eval(pampInterp, logfreqs->data, deltaAmpsPlus->data, logf, pampAcc);
+      pdPhi = gsl_interp_eval(pphaseInterp, logfreqs->data, deltaPhasesPlus->data, logf, pphaseAcc);
+      cdA = gsl_interp_eval(campInterp, logfreqs->data, deltaAmpsCross->data, logf, campAcc);
+      cdPhi = gsl_interp_eval(cphaseInterp, logfreqs->data, deltaPhasesCross->data, logf, cphaseAcc);
+    }
+
+    (*wfMargFactorROQLinPlus)->data[i] = (1.0 + pdA)*(2.0 + I*pdPhi)/(2.0 - I*pdPhi);
+    (*wfMargFactorROQLinCross)->data[i] = (1.0 + cdA)*(2.0 + I*cdPhi)/(2.0 - I*cdPhi);
+  }
+
+  for (unsigned int j = 0; j < freqNodesQuad->length; j++) {
+    REAL8 f = freqNodesQuad->data[j];
+    REAL8 logf = log(f);
+    if (f < lowf || f > highf) {
+      pdA = 0.0;
+      pdPhi = 0.0;
+      cdA = 0.0;
+      cdPhi = 0.0;
+    } else {
+      pdA = gsl_interp_eval(pampInterp, logfreqs->data, deltaAmpsPlus->data, logf, pampAcc);
+      pdPhi = gsl_interp_eval(pphaseInterp, logfreqs->data, deltaPhasesPlus->data, logf, pphaseAcc);
+      cdA = gsl_interp_eval(campInterp, logfreqs->data, deltaAmpsCross->data, logf, campAcc);
+      cdPhi = gsl_interp_eval(cphaseInterp, logfreqs->data, deltaPhasesCross->data, logf, cphaseAcc);
+    }
+
+    (*wfMargFactorROQQuadPlus)->data[j] = (1.0 + pdA)*(2.0 + I*pdPhi)/(2.0 - I*pdPhi);
+    (*wfMargFactorROQQuadCross)->data[j] = (1.0 + cdA)*(2.0 + I*cdPhi)/(2.0 - I*cdPhi);
+
+  }
+
+ cleanup:
+  if (pampInterp != NULL) gsl_interp_free(pampInterp);
+  if (pphaseInterp != NULL) gsl_interp_free(pphaseInterp);
+  if (campInterp != NULL) gsl_interp_free(campInterp);
+  if (cphaseInterp != NULL) gsl_interp_free(cphaseInterp);
+  if (pampAcc != NULL) gsl_interp_accel_free(campAcc);
+  if (pphaseAcc != NULL) gsl_interp_accel_free(cphaseAcc);
+  if (campAcc != NULL) gsl_interp_accel_free(campAcc);
+  if (cphaseAcc != NULL) gsl_interp_accel_free(cphaseAcc);
+
+  if (status == XLAL_SUCCESS) {
+    return status;
+  } else {
+    XLAL_ERROR(status, "%s", fmt);
+  }
+}
+
+int LALInferenceWFMargFactor(REAL8Vector *logfreqs,
+                             REAL8Vector *deltaAmps,
+                             REAL8Vector *deltaPhases,
+                             COMPLEX16FrequencySeries *wfMargFactor) {
+  size_t i = 0;
+  gsl_interp_accel *ampAcc = NULL, *phaseAcc = NULL;
+  gsl_interp *ampInterp = NULL, *phaseInterp = NULL;
+
+  int status = XLAL_SUCCESS;
+  const char *fmt = "";
+
+  size_t N = 0;
+
+  if (logfreqs == NULL || deltaAmps == NULL || deltaPhases == NULL || wfMargFactor == NULL) {
+    status = XLAL_EINVAL;
+    fmt = "bad input";
+    goto cleanup;
+  }
+
+  if (logfreqs->length != deltaAmps->length || deltaAmps->length != deltaPhases->length) {
+    status = XLAL_EINVAL;
+    fmt = "input lengths differ";
+    goto cleanup;
+  }
+
+  N = logfreqs->length;
+
+  ampInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+  phaseInterp = gsl_interp_alloc(gsl_interp_cspline, N);
+
+  if (ampInterp == NULL || phaseInterp == NULL) {
+    status = XLAL_ENOMEM;
+    fmt = "could not allocate GSL interpolation objects";
+    goto cleanup;
+  }
+
+  ampAcc = gsl_interp_accel_alloc();
+  phaseAcc = gsl_interp_accel_alloc();
+
+  if (ampAcc == NULL || phaseAcc == NULL) {
+    status = XLAL_ENOMEM;
+    fmt = "could not allocate interpolation acceleration objects";
+    goto cleanup;
+  }
+
+  gsl_interp_init(ampInterp, logfreqs->data, deltaAmps->data, N);
+  gsl_interp_init(phaseInterp, logfreqs->data, deltaPhases->data, N);
+
+  REAL8 lowf = exp(logfreqs->data[0]);
+  REAL8 highf = exp(logfreqs->data[N-1]);
+  for (i = 0; i < wfMargFactor->data->length; i++) {
+    REAL8 dA = 0.0, dPhi = 0.0;
+    REAL8 f = wfMargFactor->deltaF*i;
+
+    if (f < lowf || f > highf) {
+      dA = 0.0;
+      dPhi = 0.0;
+    } else {
+      dA = gsl_interp_eval(ampInterp, logfreqs->data, deltaAmps->data, log(f), ampAcc);
+      dPhi = gsl_interp_eval(phaseInterp, logfreqs->data, deltaPhases->data, log(f), phaseAcc);
+    }
+
+    wfMargFactor->data->data[i] = (1.0 + dA)*(2.0 + I*dPhi)/(2.0 - I*dPhi);
+  }
+
+ cleanup:
+  if (ampInterp != NULL) gsl_interp_free(ampInterp);
+  if (phaseInterp != NULL) gsl_interp_free(phaseInterp);
+  if (ampAcc != NULL) gsl_interp_accel_free(ampAcc);
+  if (phaseAcc != NULL) gsl_interp_accel_free(phaseAcc);
+
+  if (status == XLAL_SUCCESS) {
+    return status;
+  } else {
+    XLAL_ERROR(status, "%s", fmt);
+  }
+}
diff --git a/lalinference/src/LALInference.h b/lalinference/src/LALInference.h
index 55c8117909..ec82e32ec6 100644
--- a/lalinference/src/LALInference.h
+++ b/lalinference/src/LALInference.h
@@ -369,8 +369,24 @@ int LALInferenceSplineCalibrationFactorROQ(REAL8Vector *logfreqs,
 					REAL8Sequence *freqNodesQuad,
 					COMPLEX16Sequence **calFactorROQQuad);
 
+int LALInferenceWFMargFactorROQ(REAL8Vector *logfreqs,
+          REAL8Vector *deltaAmpsPlus,
+          REAL8Vector *deltaPhasesPlus,
+          REAL8Vector *deltaAmpsCross,
+          REAL8Vector *deltaPhasesCross,
+          REAL8Sequence *freqNodesLin,
+          COMPLEX16Sequence **wfMargFactorROQLinPlus,
+          COMPLEX16Sequence **wfMargFactorROQLinCross,
+          REAL8Sequence *freqNodesQuad,
+          COMPLEX16Sequence **wfMargFactorROQQuadPlus,
+          COMPLEX16Sequence **wfMargFactorROQQuadCross);
+
+int LALInferenceWFMargFactor(REAL8Vector *logfreqs,
+                             REAL8Vector *deltaAmps,
+                             REAL8Vector *deltaPhases,
+                             COMPLEX16FrequencySeries *wfMargFactor);
+
 
-//Wrapper for template computation
 //(relies on LAL libraries for implementation) <- could be a #DEFINE ?
 //typedef void (LALTemplateFunction) (LALInferenceVariables *currentParams, struct tagLALInferenceIFOData *data); //Parameter Set is modelParams of LALInferenceIFOData
 /**
@@ -717,6 +733,11 @@ tagLALInferenceROQModel
 
   COMPLEX16Sequence *calFactorQuadratic;
 
+  COMPLEX16Sequence *wfMargFactorROQLinPlus;
+  COMPLEX16Sequence *wfMargFactorROQLinCross;
+  COMPLEX16Sequence *wfMargFactorROQQuadPlus;
+  COMPLEX16Sequence *wfMargFactorROQQuadCross;
+
   REAL8Sequence  * frequencyNodesLinear; /** empirical frequency nodes for the likelihood. NOTE: needs to be stored from data read from command line */
   REAL8Sequence * frequencyNodesQuadratic;
   REAL8 trigtime;
diff --git a/lalinference/src/LALInferenceInit.h b/lalinference/src/LALInferenceInit.h
index fa38228b59..eefaedbf85 100644
--- a/lalinference/src/LALInferenceInit.h
+++ b/lalinference/src/LALInferenceInit.h
@@ -107,5 +107,7 @@ void LALInferenceCheckOptionsConsistency(ProcessParamsTable *commandLine);
 
 void LALInferenceInitCalibrationVariables(LALInferenceRunState *runState, LALInferenceVariables *currentParams);
 
+void LALInferenceInitWaveformMarginalisationVariables(LALInferenceRunState *runState, LALInferenceVariables *currentParams);
+
 #endif
 
diff --git a/lalinference/src/LALInferenceInitCBC.c b/lalinference/src/LALInferenceInitCBC.c
index e40f16a9a0..5bcf06b224 100644
--- a/lalinference/src/LALInferenceInitCBC.c
+++ b/lalinference/src/LALInferenceInitCBC.c
@@ -474,6 +474,97 @@ static int destroyCalibrationEnvelope(struct spcal_envelope *env)
     return XLAL_SUCCESS;
 }
 
+void LALInferenceInitWaveformMarginalisationVariables(LALInferenceRunState *runState, LALInferenceVariables *currentParams) {
+  ProcessParamsTable *ppt = NULL;
+  UINT4 wfmargOn = 1;
+  if ((ppt = LALInferenceGetProcParamVal(runState->commandLine, "--enable-spline-wfmarg"))){
+    /* Use spline to marginalize over WF uncertanties*/
+    UINT4 nmarg = 5; /* Number of marginalisation nodes, log-distributed*/
+    if ((ppt = LALInferenceGetProcParamVal(runState->commandLine, "--wfmarg-nodes"))) {
+      nmarg = atoi(ppt->value);
+      if (nmarg < 3) { /* Cannot do spline with fewer than 3 points! */
+        fprintf(stderr, "ERROR: given '--wfmarg-nodes %d', but cannot spline with fewer than 3\n", nmarg);
+        exit(1);
+      }
+    }
+    LALInferenceAddVariable(currentParams, "wfmarg_active", &wfmargOn, LALINFERENCE_UINT4_t, LALINFERENCE_PARAM_FIXED);
+    LALInferenceAddVariable(currentParams, "wfmarg_npts", &nmarg, LALINFERENCE_UINT4_t, LALINFERENCE_PARAM_FIXED);
+
+    UINT4 i;
+
+    char freqVarName[VARNAME_MAX];
+    char pampVarName[VARNAME_MAX];
+    char pphaseVarName[VARNAME_MAX];
+    char campVarName[VARNAME_MAX];
+    char cphaseVarName[VARNAME_MAX];
+
+    /* I assume all IFOs have the same bandwidth here
+    might want to change that for the future TODO*/
+    REAL8 fMin = runState->data->fLow;
+    REAL8 fMax = runState->data->fHigh;
+    REAL8 logFMin = log(fMin);
+    REAL8 logFMax = log(fMax);
+    REAL8 dLogF = (logFMax - logFMin)/(nmarg-1);
+
+    char envp_uncert_op[VARNAME_MAX];
+    char envc_uncert_op[VARNAME_MAX];
+    struct spcal_envelope *envp=NULL;
+    struct spcal_envelope *envc=NULL;
+
+    snprintf(envp_uncert_op, VARNAME_MAX, "--wfmarg-plus-envelope");
+    snprintf(envc_uncert_op, VARNAME_MAX, "--wfmarg-cross-envelope");
+
+    if( (ppt=LALInferenceGetProcParamVal(runState->commandLine, envp_uncert_op)))
+          envp = initCalibrationEnvelope(ppt->value);
+    else{
+            fprintf(stderr,"Error, missing %s\n",envp_uncert_op);
+            exit(1);
+    }
+    if( (ppt=LALInferenceGetProcParamVal(runState->commandLine, envc_uncert_op)))
+          envc = initCalibrationEnvelope(ppt->value);
+    else{
+            fprintf(stderr,"Error, missing %s\n",envc_uncert_op);
+            exit(1);
+    }
+          /* Now add each spline node */
+    for(i=0;i<nmarg;i++)
+    {
+        snprintf(freqVarName, VARNAME_MAX, "wfmarg_logfreq_%i",i);
+        REAL8 logFreq = logFMin + i*dLogF;
+        LALInferenceAddREAL8Variable(currentParams,freqVarName,logFreq,LALINFERENCE_PARAM_FIXED);
+
+        snprintf(pampVarName, VARNAME_MAX, "wfmarg_plus_amp_%i", i);
+        snprintf(pphaseVarName, VARNAME_MAX, "wfmarg_plus_phase_%i", i);
+
+        REAL8 pamp_std = gsl_spline_eval(envp->amp_std, logFreq, NULL);
+        REAL8 pamp_mean = gsl_spline_eval(envp->amp_median, logFreq, NULL);
+        REAL8 pphase_std = gsl_spline_eval(envp->phase_std, logFreq, NULL);
+        REAL8 pphase_mean = gsl_spline_eval(envp->phase_median, logFreq, NULL);
+
+        LALInferenceRegisterGaussianVariableREAL8(runState, currentParams, pampVarName, 0, pamp_mean, pamp_std, LALINFERENCE_PARAM_LINEAR);
+        LALInferenceRegisterGaussianVariableREAL8(runState, currentParams, pphaseVarName, 0, pphase_mean, pphase_std, LALINFERENCE_PARAM_LINEAR);
+
+        snprintf(campVarName, VARNAME_MAX, "wfmarg_cross_amp_%i", i);
+        snprintf(cphaseVarName, VARNAME_MAX, "wfmarg_cross_phase_%i", i);
+
+        REAL8 camp_std = gsl_spline_eval(envc->amp_std, logFreq, NULL);
+        REAL8 camp_mean = gsl_spline_eval(envc->amp_median, logFreq, NULL);
+        REAL8 cphase_std = gsl_spline_eval(envc->phase_std, logFreq, NULL);
+        REAL8 cphase_mean = gsl_spline_eval(envc->phase_median, logFreq, NULL);
+
+        LALInferenceRegisterGaussianVariableREAL8(runState, currentParams, campVarName, 0, camp_mean, camp_std, LALINFERENCE_PARAM_LINEAR);
+        LALInferenceRegisterGaussianVariableREAL8(runState, currentParams, cphaseVarName, 0, cphase_mean, cphase_std, LALINFERENCE_PARAM_LINEAR);
+    } /* End loop over spline nodes */
+
+    destroyCalibrationEnvelope(envp);
+    destroyCalibrationEnvelope(envc);
+
+  }/* End function for wf_marg nodes */
+  else {
+    return; /* no wf marginalisation needed :) */
+  }
+}
+
 void LALInferenceInitCalibrationVariables(LALInferenceRunState *runState, LALInferenceVariables *currentParams) {
   ProcessParamsTable *ppt = NULL;
   LALInferenceIFOData *ifo = NULL;
@@ -1261,6 +1352,8 @@ LALInferenceModel *LALInferenceInitCBCModel(LALInferenceRunState *state) {
   /* Handle, if present, requests for calibration parameters. */
   LALInferenceInitCalibrationVariables(state, model->params);
 
+  LALInferenceInitWaveformMarginalisationVariables(state, model->params);
+
   //Only add waveform parameters to model if needed
   if(signal_flag)
   {
diff --git a/lalinference/src/LALInferenceLikelihood.c b/lalinference/src/LALInferenceLikelihood.c
index eb1df11ce7..d00f8cc0b1 100644
--- a/lalinference/src/LALInferenceLikelihood.c
+++ b/lalinference/src/LALInferenceLikelihood.c
@@ -92,6 +92,43 @@ static int get_calib_spline(LALInferenceVariables *vars, const char *ifoname, RE
   return(XLAL_SUCCESS);
 }
 
+static int get_wfmarg_spline(LALInferenceVariables *vars, REAL8Vector **logfreqs, REAL8Vector **pamps, REAL8Vector **pphases, REAL8Vector **camps, REAL8Vector **cphases);
+static int get_wfmarg_spline(LALInferenceVariables *vars, REAL8Vector **logfreqs, REAL8Vector **pamps, REAL8Vector **pphases, REAL8Vector **camps, REAL8Vector **cphases)
+{
+  UINT4 npts = LALInferenceGetUINT4Variable(vars, "wfmarg_npts");
+  char pampname[VARNAME_MAX];
+  char pphasename[VARNAME_MAX];
+  char campname[VARNAME_MAX];
+  char cphasename[VARNAME_MAX];
+  char freqname[VARNAME_MAX];
+  if(!*logfreqs) *logfreqs = XLALCreateREAL8Vector(npts);
+  if(!*pamps) *pamps = XLALCreateREAL8Vector(npts);
+  if(!*pphases) *pphases = XLALCreateREAL8Vector(npts);
+  if(!*camps) *camps = XLALCreateREAL8Vector(npts);
+  if(!*cphases) *cphases = XLALCreateREAL8Vector(npts);
+  assert((*logfreqs)->length==npts);
+  assert((*pamps)->length==npts);
+  assert((*pphases)->length==npts);
+  assert((*camps)->length==npts);
+  assert((*cphases)->length==npts);
+
+  for(UINT4 i=0;i<npts;i++)
+  {
+    snprintf(freqname, VARNAME_MAX, "wfmarg_logfreq_%i", i);
+    snprintf(pampname, VARNAME_MAX, "wfmarg_plus_amp_%i",  i);
+    snprintf(pphasename, VARNAME_MAX, "wfmarg_plus_phase_%i",  i);
+    snprintf(campname, VARNAME_MAX, "wfmarg_cross_amp_%i",  i);
+    snprintf(cphasename, VARNAME_MAX, "wfmarg_cross_phase_%i",  i);
+
+    (*logfreqs)->data[i] = LALInferenceGetREAL8Variable(vars, freqname);
+    (*pamps)->data[i] =  LALInferenceGetREAL8Variable(vars, pampname);
+    (*pphases)->data[i] = LALInferenceGetREAL8Variable(vars, pphasename);
+    (*camps)->data[i] =  LALInferenceGetREAL8Variable(vars, campname);
+    (*cphases)->data[i] = LALInferenceGetREAL8Variable(vars, cphasename);
+  }
+  return(XLAL_SUCCESS);
+}
+
 void LALInferenceInitLikelihood(LALInferenceRunState *runState)
 {
     char help[]="\
@@ -350,9 +387,16 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
   COMPLEX16FrequencySeries *calFactor = NULL;
   COMPLEX16 calF = 0.0;
 
+  COMPLEX16FrequencySeries *wfMargFactorPlus = NULL;
+  COMPLEX16FrequencySeries *wfMargFactorCross = NULL;
+
   REAL8Vector *logfreqs = NULL;
   REAL8Vector *amps = NULL;
   REAL8Vector *phases = NULL;
+  REAL8Vector *pamps = NULL;
+  REAL8Vector *pphases = NULL;
+  REAL8Vector *camps = NULL;
+  REAL8Vector *cphases = NULL;
 
   UINT4 spcal_active = 0;
   REAL8 calamp=0.0;
@@ -361,6 +405,8 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
   REAL8 sin_calpha=sin(calpha);
   UINT4 constantcal_active=0;
 
+  UINT4 wfmarg_active = 0;
+
   /* ROQ likelihood stuff */
   REAL8 d_inner_h=0.0;
 
@@ -380,6 +426,14 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
     exit(1);
   }
 
+  if (LALInferenceCheckVariable(currentParams, "wfmarg_active") && (*(UINT4 *)LALInferenceGetVariable(currentParams, "wfmarg_active"))) {
+    wfmarg_active = 1;
+  }
+  if ((spcal_active && wfmarg_active) || (constantcal_active && wfmarg_active)){
+    fprintf(stderr,"ERROR: cannot use calibration error marginalization and Waveform marginalization together. Exiting...\n");
+    exit(1);
+  }
+
   REAL8 degreesOfFreedom=2.0;
   REAL8 chisq=0.0;
   /* margphi params */
@@ -646,6 +700,49 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
 
         /* Template is now in model->timeFreqhPlus and hCross */
 
+      if (wfmarg_active) {
+        logfreqs = NULL;
+        pamps = NULL;
+        pphases = NULL;
+        camps = NULL;
+        cphases = NULL;
+        get_wfmarg_spline(currentParams, &logfreqs, &pamps, &pphases, &camps, &cphases);
+        if (model->roq_flag) {
+
+             LALInferenceWFMargFactorROQ(logfreqs, pamps, pphases, camps, cphases,
+              model->roq->frequencyNodesLinear,
+              &(model->roq->wfMargFactorROQLinPlus),
+              &(model->roq->wfMargFactorROQLinCross),
+              model->roq->frequencyNodesQuadratic,
+              &(model->roq->wfMargFactorROQQuadPlus),
+              &(model->roq->wfMargFactorROQQuadCross));
+        }
+        else{
+          if (wfMargFactorPlus == NULL) {
+            wfMargFactorPlus = XLALCreateCOMPLEX16FrequencySeries("plus wf marginalization factors",
+                       &(dataPtr->freqData->epoch),
+                       0, dataPtr->freqData->deltaF,
+                       &lalDimensionlessUnit,
+                       dataPtr->freqData->data->length);
+          }
+          if (wfMargFactorCross == NULL) {
+            wfMargFactorCross = XLALCreateCOMPLEX16FrequencySeries("cross wf marginalization factors",
+                       &(dataPtr->freqData->epoch),
+                       0, dataPtr->freqData->deltaF,
+                       &lalDimensionlessUnit,
+                       dataPtr->freqData->data->length);
+          }
+          LALInferenceWFMargFactor(logfreqs, pamps, pphases, wfMargFactorPlus);
+          LALInferenceWFMargFactor(logfreqs, camps, cphases, wfMargFactorCross);
+      }
+      if(logfreqs) XLALDestroyREAL8Vector(logfreqs);
+      if(pamps) XLALDestroyREAL8Vector(pamps);
+      if(pphases) XLALDestroyREAL8Vector(pphases);
+      if(camps) XLALDestroyREAL8Vector(camps);
+      if(cphases) XLALDestroyREAL8Vector(cphases);
+
+      }
+
         /* Calibration stuff if necessary */
         /*spline*/
         if (spcal_active) {
@@ -784,7 +881,23 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
 			this_ifo_s += dataPtr->roq->weightsQuadratic[jjj] * creal( conj( model->roq->calFactorQuadratic->data[jjj] * (model->roq->hptildeQuadratic->data->data[jjj]*dataPtr->fPlus + model->roq->hctildeQuadratic->data->data[jjj]*dataPtr->fCross) ) * ( model->roq->calFactorQuadratic->data[jjj] * (model->roq->hptildeQuadratic->data->data[jjj]*dataPtr->fPlus + model->roq->hctildeQuadratic->data->data[jjj]*dataPtr->fCross) ) );
 		}
 	}
+        else if (wfmarg_active) {
+
+            for(unsigned int iii=0; iii < model->roq->frequencyNodesLinear->length; iii++){
+
+                 complex double template_EI = (dataPtr->fPlus*model->roq->hptildeLinear->data->data[iii]*model->roq->wfMargFactorROQLinPlus->data[iii] + dataPtr->fCross*model->roq->hctildeLinear->data->data[iii]*model->roq->wfMargFactorROQLinCross->data[iii] );
+
+                 weight_iii = gsl_spline_eval (dataPtr->roq->weights_linear[iii].spline_real_weight_linear, timeshift, dataPtr->roq->weights_linear[iii].acc_real_weight_linear) + I*gsl_spline_eval (dataPtr->roq->weights_linear[iii].spline_imag_weight_linear, timeshift, dataPtr->roq->weights_linear[iii].acc_imag_weight_linear);
+
+                 this_ifo_d_inner_h += ( weight_iii * ( conj( template_EI ) ) );
+            }
 
+            for(unsigned int jjj=0; jjj < model->roq->frequencyNodesQuadratic->length; jjj++){
+
+                 this_ifo_s += dataPtr->roq->weightsQuadratic[jjj] * creal( conj(dataPtr->fPlus*model->roq->hptildeQuadratic->data->data[jjj]*model->roq->wfMargFactorROQQuadPlus->data[jjj] + dataPtr->fCross*model->roq->hctildeQuadratic->data->data[jjj]*model->roq->wfMargFactorROQQuadCross->data[jjj]) * (dataPtr->fPlus*model->roq->hptildeQuadratic->data->data[jjj]*model->roq->wfMargFactorROQQuadPlus->data[jjj] + dataPtr->fCross*model->roq->hctildeQuadratic->data->data[jjj]*model->roq->wfMargFactorROQQuadCross->data[jjj]) );
+            }
+
+        }
 	else{
 
 		for(unsigned int iii=0; iii < model->roq->frequencyNodesLinear->length; iii++){
@@ -904,7 +1017,15 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
 
       if(signalFlag){
       /* derive template (involving location/orientation parameters) from given plus/cross waveforms: */
-      COMPLEX16 plainTemplate = Fplus*(*hptilde)+Fcross*(*hctilde);
+
+      COMPLEX16 plainTemplate = 0.0;
+
+      if (wfmarg_active) {
+          plainTemplate = Fplus*(*hptilde)*wfMargFactorPlus->data->data[i]+Fcross*(*hctilde)*wfMargFactorCross->data->data[i];
+        }
+      else {
+        plainTemplate = Fplus*(*hptilde)+Fcross*(*hctilde);
+      }
 
       /* Do time shifting */
       template = plainTemplate * (re + I*im);
@@ -1022,6 +1143,14 @@ static REAL8 LALInferenceFusedFreqDomainLogLikelihood(LALInferenceVariables *cur
       XLALDestroyCOMPLEX16FrequencySeries(calFactor);
       calFactor = NULL;
     }
+    if (!(wfMargFactorPlus == NULL)) {
+      XLALDestroyCOMPLEX16FrequencySeries(wfMargFactorPlus);
+      wfMargFactorPlus = NULL;
+    }
+    if (!(wfMargFactorCross == NULL)) {
+      XLALDestroyCOMPLEX16FrequencySeries(wfMargFactorCross);
+      wfMargFactorCross = NULL;
+    }
   } /* end loop over detectors */
 
   }
-- 
2.16.2

