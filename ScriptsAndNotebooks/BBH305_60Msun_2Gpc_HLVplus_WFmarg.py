#!/usr/bin/env python
"""

This tutorial must be run on a LIGO cluster.
"""
import matplotlib as mpl
mpl.use('Agg')
import bilby
from bilby.core.prior import Prior
import pdb
import numpy as np
import gwsurrogate
import json
from scipy.signal.windows import tukey
from scipy.stats import truncnorm, norm
from scipy.interpolate import interp1d

MargSurr = True

#gwsurrogate.catalog.pull('NRSur7dq4')
sur = gwsurrogate.LoadSurrogate('NRHybSur3dq8')

if MargSurr:
	trunc_sur = gwsurrogate.LoadSurrogate('/home/carl-johan.haster/projects/Waveform_marginalisation/TruncatedSurrogate/NRHybSur3dq8.h5')

def NRHybSur3dq8_marg(times, total_mass, mass_ratio, chi_1, chi_2, luminosity_distance, theta_jn, phase, **kwargs):
	#set up the waveform arguments
	reference_frequency = kwargs['reference_frequency']
	dt = 1./kwargs['sampling_frequency']
	ellMax = kwargs['ellMax']
	minimum_frequency = kwargs['minimum_frequency']

	#convert parameters
	q = 1./mass_ratio
	# q is > 1, whereas mass_ratio is < 1
	spin1 = [0., 0., chi_1]
	spin2 = [0., 0., chi_2]
	
	#evaluate waveform
	t, h, dyn = sur(q, spin1, spin2, dt=dt, f_low=minimum_frequency, f_ref=reference_frequency,
			ellMax=ellMax, dist_mpc=luminosity_distance, M=total_mass,
			inclination=theta_jn, phi_ref=phase, units='mks')
	if MargSurr:
		t_trunc, h_trunc, dyn_trunc = trunc_sur(q, spin1, spin2, dt=dt, f_low=minimum_frequency, f_ref=reference_frequency,
			ellMax=ellMax, dist_mpc=luminosity_distance, M=total_mass,
			inclination=theta_jn, phi_ref=phase, units='mks')

		if len(h.real) > len(h_trunc.real):
			h_plus_diff = h.real[len(h.real) - len(h_trunc.real):] - h_trunc.real
			h_cross_diff = -h.imag[len(h.imag) - len(h_trunc.imag):] + h_trunc.imag
		elif len(h.real) < len(h_trunc.real):
			h_plus_diff = np.pad(h.real, (len(h_trunc.real) - len(h.real), 0), 'constant', constant_values=0.) - h_trunc.real
			h_cross_diff = np.pad(-h.imag, (len(h_trunc.imag) - len(h.imag), 0), 'constant', constant_values=0.) + h_trunc.imag
		else:
			h_plus_diff = h.real - h_trunc.real
			h_cross_diff = -h.imag + h_trunc.imag

		h_plus_diff_fft, h_plus_diff_freq = bilby.core.utils.nfft(h_plus_diff, sampling_frequency=1./dt)
		#print(h_plus_diff_freq.shape, h_plus_diff_freq)
		Raw_diff_plus_amp = np.absolute(h_plus_diff_fft)
		Raw_diff_plus_phase = np.unwrap(np.angle(h_plus_diff_fft))

		h_cross_diff_fft, h_cross_diff_freq = bilby.core.utils.nfft(h_cross_diff, sampling_frequency=1./dt)
		Raw_diff_cross_amp = np.absolute(h_cross_diff_fft)
		Raw_diff_cross_phase = np.unwrap(np.angle(h_cross_diff_fft))

		Num_Select_raw = int(len(h_plus_diff)/100.)
		f_select_array = np.logspace(np.log10(h_plus_diff_freq[1]), np.log10(h_plus_diff_freq[-1]), Num_Select_raw)
		f_select_array_indeces_raw = find_nearest_indices(f_select_array, h_plus_diff_freq)
		f_select_array_indeces = np.array(list(dict.fromkeys(f_select_array_indeces_raw.tolist())))
		Num_Select = len(f_select_array_indeces)

		Amp_mean_plus = Raw_diff_plus_amp[f_select_array_indeces]
		Amp_std_plus = Raw_diff_plus_amp[f_select_array_indeces]/3.
		Phase_mean_plus = Raw_diff_plus_phase[f_select_array_indeces]
		Phase_std_plus = np.ones(Num_Select)

		Amp_mean_cross = Raw_diff_cross_amp[f_select_array_indeces]
		Amp_std_cross = Raw_diff_cross_amp[f_select_array_indeces]/3.
		Phase_mean_cross = Raw_diff_cross_phase[f_select_array_indeces]
		Phase_std_cross = np.ones(Num_Select)

		Amp_Gauss_plus = np.zeros(Num_Select)
		Phase_Gauss_plus = np.zeros(Num_Select)
		Amp_Gauss_cross = np.zeros(Num_Select)
		Phase_Gauss_cross = np.zeros(Num_Select)

		for i in range(Num_Select):
			low_plus, high_plus = 0., 5*Amp_mean_plus[i]
			low_a_plus = (low_plus-Amp_mean_plus[i])/Amp_std_plus[i]
			high_b_plus = (high_plus-Amp_mean_plus[i])/Amp_std_plus[i]
			Amp_Gauss_plus[i] = truncnorm.rvs(a=low_a_plus, b=high_b_plus, \
				loc = Amp_mean_plus[i], scale=Amp_std_plus[i], size=1)
			Phase_Gauss_plus[i] = norm.rvs(loc = Phase_mean_plus[i], scale=Phase_std_plus[i], size=1)

			low_cross, high_cross = 0., 5*Amp_mean_cross[i]
			low_a_cross = (low_plus-Amp_mean_cross[i])/Amp_std_cross[i]
			high_b_cross = (high_plus-Amp_mean_cross[i])/Amp_std_cross[i]
			Amp_Gauss_cross[i] = truncnorm.rvs(a=low_a_cross, b=high_b_cross, \
				loc = Amp_mean_cross[i], scale=Amp_std_cross[i], size=1)
			Phase_Gauss_cross[i] = norm.rvs(loc = Phase_mean_cross[i], scale=Phase_std_cross[i], size=1)

		Amp_plus_interpolant = interp1d(h_plus_diff_freq[f_select_array_indeces], Amp_Gauss_plus, \
			kind='cubic', bounds_error=False, fill_value = 0.)
		Phase_plus_interpolant = interp1d(h_plus_diff_freq[f_select_array_indeces], Phase_Gauss_plus, \
			kind='cubic', bounds_error=False, fill_value = 0.)
		Freq_domain_diff_plus = Amp_plus_interpolant(h_plus_diff_freq)*np.exp(1j*Amp_plus_interpolant(h_plus_diff_freq))
		Time_domain_diff_plus = bilby.core.utils.infft(frequency_domain_strain=Freq_domain_diff_plus, sampling_frequency=1/dt)
		if len(Time_domain_diff_plus) > len(h_trunc.real):
			Time_domain_diff_plus = Time_domain_diff_plus[:len(h_trunc.real)]
		elif len(Time_domain_diff_plus) < len(h_trunc.real):
			Time_domain_diff_plus = np.pad(Time_domain_diff_plus, (len(h_trunc.real) - len(Time_domain_diff_plus), 0), 'constant')

		Amp_cross_interpolant = interp1d(h_plus_diff_freq[f_select_array_indeces], Amp_Gauss_cross, \
			kind='cubic', bounds_error=False, fill_value = 0.)
		Phase_cross_interpolant = interp1d(h_plus_diff_freq[f_select_array_indeces], Phase_Gauss_cross, \
			kind='cubic', bounds_error=False, fill_value = 0.)
		Freq_domain_diff_cross = Amp_plus_interpolant(h_plus_diff_freq)*np.exp(1j*Amp_plus_interpolant(h_plus_diff_freq))
		Time_domain_diff_cross = bilby.core.utils.infft(frequency_domain_strain=Freq_domain_diff_cross, sampling_frequency=1/dt)
		if len(Time_domain_diff_cross) > len(h_trunc.imag):
			Time_domain_diff_cross = Time_domain_diff_cross[:len(h_trunc.imag)]
		elif len(Time_domain_diff_cross) < len(h_trunc.imag):
			Time_domain_diff_cross = np.pad(Time_domain_diff_cross, (len(h_trunc.imag) - len(Time_domain_diff_cross), 0), 'constant')

		h_plus = h_trunc.real + Time_domain_diff_plus
		h_cross = -1*h_trunc.imag + Time_domain_diff_cross

		diff = len(times) - len(t_trunc)

	else:
		h_plus = np.real(h)
		h_cross = -1*np.imag(h)
	
		#zero pad
		diff = len(times) - len(t)

	if diff > 0:
		# if the waveform is shorter, zero pad to end
		h_plus = np.pad(h_plus, (0, diff), 'constant')
		h_cross = np.pad(h_cross, (0, diff), 'constant')
	elif diff < 0:
		# if the waveform is longer, remove from inspiral
		h_plus = h_plus[-diff:]
		h_cross = h_cross[-diff:]
		if MargSurr:
			t_trunc = t_trunc[-diff:]
		else:
		    t = t[-diff:]
	if MargSurr:
		ind_t0 = np.where(t_trunc>=0)[0][0]
	else:
		ind_t0 = np.where(t>=0)[0][0]

	#tukey window the padded waveform
	duration = len(times)*dt
	alpha = 2*0.2/duration #rise time of window is 0.2s 
	middle = int(len(times)/2)
	window = tukey(len(times), alpha)
	window[middle:] = 1 #only window the inspiral
	h_plus *=window
	h_cross *= window

	#timeshift peak to beginning of segment
	h_plus = np.roll(h_plus, len(h_plus)-ind_t0)
	h_cross = np.roll(h_cross, len(h_cross)-ind_t0)
	#print('BO  ',h_plus, h_plus)
	return {'plus': h_plus, 'cross': h_cross}

def find_nearest_indices(values, array):
	return np.absolute(np.subtract.outer(array, values)).argmin(0)
	
label = 'TestInj_305_60Msun_NRHybSur3dq8_marg'
outdir = '/home/carl-johan.haster/projects/Waveform_marginalisation/TestRuns/' + label 

detectors = ['H1', 'L1', 'V1']

# Duration of data around the event to use
duration = 4

# Duration of PSD data
psd_duration = 1024

# Time of event
trigger_time = 1187008882.445699930

# Minimum frequency and reference frequency
minimum_frequency = 20.  # Hz
reference_frequency = 20.
sampling_frequency = 4096.

gps_start_time = trigger_time + 2. - duration

#prior = bilby.gw.prior.PriorDict(filename='/home/sylvia.biscoveanu/bilby_fork/examples/open_data_examples/S190521g/heavy.prior')
prior = bilby.gw.prior.CBCPriorDict()

prior['total_mass'] = bilby.core.prior.Uniform(name='total_mass',minimum=30., maximum=90.)
prior['mass_ratio'] = bilby.core.prior.Uniform(name='mass_ratio',minimum=0.125, maximum=1.)

prior['chi_1'] = bilby.core.prior.Uniform(name='chi_1', minimum=-0.8, maximum=0.8)
prior['chi_2'] = bilby.core.prior.Uniform(name='chi_2', minimum=-0.8, maximum=0.8)

prior['luminosity_distance'] = bilby.gw.prior.UniformComovingVolume(minimum=10, maximum=10000, name='luminosity_distance')

prior['azimuth'] = bilby.core.prior.Uniform(name='azimuth', minimum=0, maximum=2*np.pi, boundary='periodic', latex_label='$\\alpha$')
prior['zenith'] = bilby.core.prior.Sine(name='zenith', latex_label='$\\zeta$')

prior['psi'] = bilby.core.prior.Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic')
prior['theta_jn'] = bilby.core.prior.Sine(name='theta_jn')

prior['phase'] = bilby.core.prior.Uniform(name='phase', minimum=0, maximum=2 * np.pi, boundary='periodic')
prior['geocent_time'] = bilby.core.prior.Uniform(minimum=trigger_time-0.1, maximum=trigger_time+0.1, name='geocent_time', unit='$s$')

# Set up interferometer objects from the cache files
interferometers = bilby.gw.detector.InterferometerList(detectors)

inj_data_path = '/home/carl-johan.haster/projects/Waveform_marginalisation/Injection_data/TestInj_305_60Msun/'
inj_data_baseFile = 'TestInj_305_60Msun.hdf5'

for interferometer in interferometers:
	freqs, real, imag = np.loadtxt(inj_data_path+'/'+inj_data_baseFile+interferometer.name+'-freqDataWithInjection.dat', unpack=True)#, skiprows=1)
  
	strain = real + 1j*imag
	#print(len(freqs), len(strain))
	#interferometer.set_strain_data_from_frequency_domain_strain(strain,sampling_frequency=sampling_frequency, 
	#						duration=duration, start_time = gps_start_time)
	#print('kjhb',gps_start_time)
	interferometer.set_strain_data_from_frequency_domain_strain(strain, frequency_array=freqs, start_time = gps_start_time)
	interferometer.duration = duration
	interferometer.maximum_frequency = sampling_frequency/2.
	interferometer.minimum_frequency = minimum_frequency
	interferometer.sampling_frequency = sampling_frequency
	interferometer.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(
		psd_file=inj_data_path+'/'+inj_data_baseFile+interferometer.name+'-PSD.dat')

#print(interferometers.start_time, 'kjshadgfkjasd')
# Plot the strain data
interferometers.plot_data(outdir=outdir, label=label)

# OVERVIEW OF APPROXIMANTS:
# https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/Waveforms/Overview
waveform_arguments = {'reference_frequency': reference_frequency, 
						'minimum_frequency' : minimum_frequency,
						'sampling_frequency': sampling_frequency,
						'ellMax': None}

waveform_generator = bilby.gw.WaveformGenerator(
	duration=duration, 
	sampling_frequency=sampling_frequency,
	start_time=gps_start_time,
	time_domain_source_model=NRHybSur3dq8_marg,
	waveform_arguments=waveform_arguments)

# CHOOSE LIKELIHOOD FUNCTION
# Time marginalisation uses FFT and can use a prior file.
# Distance marginalisation uses a look up table calculated at run time.
# Phase marginalisation is done analytically using a Bessel function.
# If prior given, used in the distance and phase marginalization.
likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
	interferometers, waveform_generator, 
	time_marginalization=True,
	distance_marginalization=True, 
	phase_marginalization=False,
	reference_frame="H1L1",
	time_reference="geocent",
	priors=prior)

# RUN SAMPLER
# Can use log_likelihood_ratio, rather than just the log_likelihood.
# If using simulated data, pass a dictionary of injection parameters.
# A function can be specified in 'conversion_function' and applied
# to posterior to generate additional parameters e.g. source-frame masses.

# Implemented Samplers:
# LIST OF AVAILABLE SAMPLERS: Run -> bilby.sampler.implemented_samplers
npoints = 2000  # number of live points for the MCMC (dynesty)
sampler = 'pymultinest'
nthreads = 8
# Different samplers can have different additional kwargs,
# visit https://lscsoft.docs.ligo.org/bilby/samplers.html for details.

result = bilby.run_sampler(
	likelihood, prior, outdir=outdir, label=label,
	sampler=sampler, npoints=npoints, use_ratio=False,
	injection_parameters=None, nthreads=nthreads,
	conversion_function=bilby.gw.conversion.generate_all_bbh_parameters)

result.plot_corner()

