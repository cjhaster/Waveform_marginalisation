#!/usr/bin/env python
"""
Tutorial to demonstrates reading and plotting the GraceDB data for GW170608,
the lightest GWTC-1 BBH event.

This tutorial must be run on a LIGO cluster.
"""
import matplotlib as mpl
mpl.use('Agg')
import bilby
from bilby.core.prior import Prior
import pdb
import numpy as np
import gwsurrogate
import json
from scipy.signal.windows import tukey

#gwsurrogate.catalog.pull('NRSur7dq4')
sur = gwsurrogate.LoadSurrogate('NRSur7dq4')

def NRSur7dq4(times, total_mass, mass_ratio, a_1, a_2, luminosity_distance, theta_jn, phase,
	      tilt_1, tilt_2, phi_12, phi_jl, **kwargs):
	#set up the waveform arguments
	reference_frequency = kwargs['reference_frequency']
	dt = 1./kwargs['sampling_frequency']
	ellMax = kwargs['ellMax']

	#convert parameters
	mass_1, mass_2 = bilby.gw.conversion.total_mass_and_mass_ratio_to_component_masses(
		mass_ratio, total_mass)
	iota, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, = \
		bilby.gw.conversion.bilby_to_lalsimulation_spins(
		theta_jn=theta_jn, phi_jl=phi_jl, tilt_1=tilt_1, tilt_2=tilt_2,
	        phi_12=phi_12, a_1=a_1, a_2=a_2, mass_1=mass_1, mass_2=mass_2,
        	reference_frequency=reference_frequency, phase=phase)
	q = 1./mass_ratio
	spin1 = [spin1x, spin1y, spin1z]
	spin2 = [spin2x, spin2y, spin2z]
	
	#evaluate waveform
	t, h, dyn = sur(q, spin1, spin2, dt=dt, f_low=0, f_ref=reference_frequency,
			ellMax=ellMax, dist_mpc=luminosity_distance, M=total_mass,
			inclination=iota, phi_ref=phase, units='mks')
	h_plus = np.real(h)
	h_cross = -1*np.imag(h)
	
	#zero pad
	diff = len(times) - len(t)
	if diff > 0:
		# if the waveform is shorter, zero pad to end
		h_plus = np.pad(h_plus, (0, diff), 'constant')
		h_cross = np.pad(h_cross, (0, diff), 'constant')
	elif diff < 0:
		# if the waveform is longer, remove from inspiral
		h_plus = h_plus[-diff:]
		h_cross = h_cross[-diff:]
		t = t[-diff:]

	#tukey window the padded waveform
	duration = len(times)*dt
	alpha = 2*0.2/duration #rise time of window is 0.2s 
	middle = len(times)/2
	window = tukey(len(times), alpha)
	window[middle:] = 1 #only window the inspiral
	h_plus *=window
	h_cross *= window

	#timeshift peak to beginning of segment
	ind_t0 = np.where(t>=0)[0][0]
	h_plus = np.roll(h_plus, len(h_plus)-ind_t0)
	h_cross = np.roll(h_cross, len(h_cross)-ind_t0)
	return {'plus': h_plus, 'cross': h_cross}

	
label = 'S190521g'
outdir = '/home/sylvia.biscoveanu/bilby_fork/examples/open_data_examples/'+ label 
# List of detectors involved in the event. GW170608 is pre-August 2017 so only
# H1 and L1 were involved
detectors = ['H1', 'L1', 'V1']
# Names of the channels and query types to use, and the event GraceDB ID - ref.
# https://ldas-jobs.ligo.caltech.edu/~eve.chase/monitor/online_pe/C02_clean/
# C02_HL_Pv2/lalinferencenest/IMRPhenomPv2pseudoFourPN/config.ini
# Calibration number to inform bilby's guess of query type if those provided are
# not recognised
calibration = 2
# Duration of data around the event to use
duration = 4
# Duration of PSD data
psd_duration = 1024
# Time of event, stored in bilby
event = json.load(open(outdir+'/G333631.json'))
trigger_time = event['gpstime']
# Minimum frequency and reference frequency
minimum_frequency = 11.  # Hz
reference_frequency = 20.
sampling_frequency = 1024.
gps_start_time = trigger_time + 2. - duration

prior = bilby.gw.prior.PriorDict(filename='/home/sylvia.biscoveanu/bilby_fork/examples/open_data_examples/S190521g/heavy.prior')
prior['geocent_time'] = bilby.core.prior.Uniform(trigger_time-0.1, trigger_time+0.1, name='geocent_time', unit='$s$')

# Set up interferometer objects from the cache files
interferometers = bilby.gw.detector.InterferometerList(detectors)

for interferometer in interferometers:
    freqs, real, imag = np.loadtxt(outdir+'/{}_{}_frequency_domain_data.dat'.format(interferometer.name, label), unpack=True, skiprows=1)
    strain = real + 1j*imag
    interferometer.set_strain_data_from_frequency_domain_strain(strain,sampling_frequency=sampling_frequency, 
							duration=duration, start_time = gps_start_time)
    interferometer.duration = duration
    interferometer.maximum_frequency = sampling_frequency/2.
    interferometer.minimum_frequency = minimum_frequency
    interferometer.sampling_frequency = sampling_frequency
    interferometer.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(
        psd_file=outdir+'/glitch_median_PSD_forLI_{}.dat'.format(interferometer.name))
    interferometer.calibration_model = bilby.gw.calibration.CubicSpline(
	prefix='recalib_{}_'.format(interferometer.name),
	minimum_frequency=interferometer.minimum_frequency,
	maximum_frequency=interferometer.maximum_frequency, n_points=10)
    calib_prior = bilby.gw.prior.CalibrationPriorDict().from_envelope_file(
	outdir+'/{}_CalEnv.txt'.format(interferometer.name),
	minimum_frequency = interferometer.minimum_frequency,
	maximum_frequency = interferometer.maximum_frequency,
	n_nodes=10, label=interferometer.name)
    prior.update(calib_prior)

# Plot the strain data
interferometers.plot_data(outdir=outdir, label=label)

# OVERVIEW OF APPROXIMANTS:
# https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/Waveforms/Overview
waveform_arguments = {'reference_frequency': reference_frequency, 
		      'sampling_frequency': sampling_frequency,
        	      'ellMax': None}

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    start_time=gps_start_time,
    time_domain_source_model=NRSur7dq4,
    waveform_arguments=waveform_arguments)

# CHOOSE LIKELIHOOD FUNCTION
# Time marginalisation uses FFT and can use a prior file.
# Distance marginalisation uses a look up table calculated at run time.
# Phase marginalisation is done analytically using a Bessel function.
# If prior given, used in the distance and phase marginalization.
likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    interferometers, waveform_generator, time_marginalization=True,
    distance_marginalization=True, phase_marginalization=False,
    priors=prior)

# RUN SAMPLER
# Can use log_likelihood_ratio, rather than just the log_likelihood.
# If using simulated data, pass a dictionary of injection parameters.
# A function can be specified in 'conversion_function' and applied
# to posterior to generate additional parameters e.g. source-frame masses.

# Implemented Samplers:
# LIST OF AVAILABLE SAMPLERS: Run -> bilby.sampler.implemented_samplers
npoints = 2000  # number of live points for the MCMC (dynesty)
sampler = 'cpnest'
nthreads = 10
# Different samplers can have different additional kwargs,
# visit https://lscsoft.docs.ligo.org/bilby/samplers.html for details.

result = bilby.run_sampler(
    likelihood, prior, outdir=outdir, label=label,
    sampler=sampler, npoints=npoints, use_ratio=False,
    injection_parameters=None, nthreads=nthreads,
    conversion_function=bilby.gw.conversion.generate_all_bbh_parameters)

result.plot_corner()

