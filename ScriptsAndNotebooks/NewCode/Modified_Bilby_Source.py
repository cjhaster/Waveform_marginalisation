import numpy as np

from ..core import utils
from ..core.utils import logger
from .conversion import bilby_to_lalsimulation_spins
from .utils import (lalsim_GetApproximantFromString,
                    lalsim_SimInspiralFD,
                    lalsim_SimInspiralChooseFDWaveform,
                    lalsim_SimInspiralWaveformParamsInsertTidalLambda1,
                    lalsim_SimInspiralWaveformParamsInsertTidalLambda2,
                    lalsim_SimInspiralChooseFDWaveformSequence)


def lal_binary_black_hole(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, **kwargs):
    """ A Binary Black Hole waveform model using lalsimulation

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the two component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total binary angular momentum and the
        orbital angular momentum
    theta_jn: float
        Angle between the total binary angular momentum and the line of sight
    phase: float
        The phase at coalescence
    kwargs: dict
        Optional keyword arguments
        Supported arguments:

        - waveform_approximant
        - reference_frequency
        - minimum_frequency
        - maximum_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.
        - lal_waveform_dictionary:
          A dictionary (lal.Dict) of arguments passed to the lalsimulation
          waveform generator. The arguments are specific to the waveform used.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomPv2', reference_frequency=50.0,
        minimum_frequency=20.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)
    return _base_lal_cbc_fd_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_12=phi_12,
        phi_jl=phi_jl, **waveform_kwargs)


def lal_binary_neutron_star(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, lambda_1, lambda_2,
        **kwargs):
    """ A Binary Neutron Star waveform model using lalsimulation

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the two component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total binary angular momentum and the
        orbital angular momentum
    theta_jn: float
        Orbital inclination
    phase: float
        The phase at coalescence
    lambda_1: float
        Dimensionless tidal deformability of mass_1
    lambda_2: float
        Dimensionless tidal deformability of mass_2
    kwargs: dict
        Optional keyword arguments
        Supported arguments:

        - waveform_approximant
        - reference_frequency
        - minimum_frequency
        - maximum_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomPv2_NRTidal', reference_frequency=50.0,
        minimum_frequency=20.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)
    return _base_lal_cbc_fd_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_12=phi_12,
        phi_jl=phi_jl, lambda_1=lambda_1, lambda_2=lambda_2, **waveform_kwargs)


def lal_eccentric_binary_black_hole_no_spins(
        frequency_array, mass_1, mass_2, eccentricity, luminosity_distance,
        theta_jn, phase, **kwargs):
    """ Eccentric binary black hole waveform model using lalsimulation (EccentricFD)

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    eccentricity: float
        The orbital eccentricity of the system
    luminosity_distance: float
        The luminosity distance in megaparsec
    theta_jn: float
        Orbital inclination
    phase: float
        The phase at coalescence
    kwargs: dict
        Optional keyword arguments
        Supported arguments:

        - waveform_approximant
        - reference_frequency
        - minimum_frequency
        - maximum_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='EccentricFD', reference_frequency=10.0,
        minimum_frequency=10.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)
    return _base_lal_cbc_fd_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        eccentricity=eccentricity, **waveform_kwargs)


def _base_lal_cbc_fd_waveform(
        frequency_array, mass_1, mass_2, luminosity_distance, theta_jn, phase,
        a_1=0.0, a_2=0.0, tilt_1=0.0, tilt_2=0.0, phi_12=0.0, phi_jl=0.0,
        lambda_1=0.0, lambda_2=0.0, eccentricity=0.0, **waveform_kwargs):
    """ Generate a cbc waveform model using lalsimulation

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total and orbital angular momenta
    theta_jn: float
        Orbital inclination
    phase: float
        The phase at coalescence
    eccentricity: float
        Binary eccentricity
    lambda_1: float
        Tidal deformability of the more massive object
    lambda_2: float
        Tidal deformability of the less massive object
    kwargs: dict
        Optional keyword arguments

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    import lal
    import lalsimulation as lalsim

    waveform_approximant = waveform_kwargs['waveform_approximant']
    reference_frequency = waveform_kwargs['reference_frequency']
    minimum_frequency = waveform_kwargs['minimum_frequency']
    maximum_frequency = waveform_kwargs['maximum_frequency']
    catch_waveform_errors = waveform_kwargs['catch_waveform_errors']
    pn_spin_order = waveform_kwargs['pn_spin_order']
    pn_tidal_order = waveform_kwargs['pn_tidal_order']
    pn_phase_order = waveform_kwargs['pn_phase_order']
    pn_amplitude_order = waveform_kwargs['pn_amplitude_order']
    waveform_dictionary = waveform_kwargs.get(
        'lal_waveform_dictionary', lal.CreateDict()
    )

    approximant = lalsim_GetApproximantFromString(waveform_approximant)

    if pn_amplitude_order != 0:
        start_frequency = lalsim.SimInspiralfLow2fStart(
            minimum_frequency, int(pn_amplitude_order), approximant)
    else:
        start_frequency = minimum_frequency

    delta_frequency = frequency_array[1] - frequency_array[0]

    frequency_bounds = ((frequency_array >= minimum_frequency) *
                        (frequency_array <= maximum_frequency))

    luminosity_distance = luminosity_distance * 1e6 * utils.parsec
    mass_1 = mass_1 * utils.solar_mass
    mass_2 = mass_2 * utils.solar_mass

    iota, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y, spin_2z = bilby_to_lalsimulation_spins(
        theta_jn=theta_jn, phi_jl=phi_jl, tilt_1=tilt_1, tilt_2=tilt_2,
        phi_12=phi_12, a_1=a_1, a_2=a_2, mass_1=mass_1, mass_2=mass_2,
        reference_frequency=reference_frequency, phase=phase)

    longitude_ascending_nodes = 0.0
    mean_per_ano = 0.0

    lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(
        waveform_dictionary, int(pn_spin_order))
    lalsim.SimInspiralWaveformParamsInsertPNTidalOrder(
        waveform_dictionary, int(pn_tidal_order))
    lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(
        waveform_dictionary, int(pn_phase_order))
    lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(
        waveform_dictionary, int(pn_amplitude_order))
    lalsim_SimInspiralWaveformParamsInsertTidalLambda1(
        waveform_dictionary, lambda_1)
    lalsim_SimInspiralWaveformParamsInsertTidalLambda2(
        waveform_dictionary, lambda_2)

    for key, value in waveform_kwargs.items():
        func = getattr(lalsim, "SimInspiralWaveformParamsInsert" + key, None)
        if func is not None:
            func(waveform_dictionary, value)

    if waveform_kwargs.get('numerical_relativity_file', None) is not None:
        lalsim.SimInspiralWaveformParamsInsertNumRelData(
            waveform_dictionary, waveform_kwargs['numerical_relativity_file'])

    if ('mode_array' in waveform_kwargs) and waveform_kwargs['mode_array'] is not None:
        mode_array = waveform_kwargs['mode_array']
        mode_array_lal = lalsim.SimInspiralCreateModeArray()
        for mode in mode_array:
            lalsim.SimInspiralModeArrayActivateMode(mode_array_lal, mode[0], mode[1])
        lalsim.SimInspiralWaveformParamsInsertModeArray(waveform_dictionary, mode_array_lal)

    if lalsim.SimInspiralImplementedFDApproximants(approximant):
        wf_func = lalsim_SimInspiralChooseFDWaveform
    else:
        wf_func = lalsim_SimInspiralFD
    try:
        hplus, hcross = wf_func(
            mass_1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
            spin_2z, luminosity_distance, iota, phase,
            longitude_ascending_nodes, eccentricity, mean_per_ano, delta_frequency,
            start_frequency, maximum_frequency, reference_frequency,
            waveform_dictionary, approximant)
    except Exception as e:
        if not catch_waveform_errors:
            raise
        else:
            EDOM = (e.args[0] == 'Internal function call failed: Input domain error')
            if EDOM:
                failed_parameters = dict(mass_1=mass_1, mass_2=mass_2,
                                         spin_1=(spin_1x, spin_2y, spin_1z),
                                         spin_2=(spin_2x, spin_2y, spin_2z),
                                         luminosity_distance=luminosity_distance,
                                         iota=iota, phase=phase,
                                         eccentricity=eccentricity,
                                         start_frequency=start_frequency)
                logger.warning("Evaluating the waveform failed with error: {}\n".format(e) +
                               "The parameters were {}\n".format(failed_parameters) +
                               "Likelihood will be set to -inf.")
                return None
            else:
                raise

    h_plus = np.zeros_like(frequency_array, dtype=complex)
    h_cross = np.zeros_like(frequency_array, dtype=complex)

    if len(hplus.data.data) > len(frequency_array):
        logger.debug("LALsim waveform longer than bilby's `frequency_array`" +
                     "({} vs {}), ".format(len(hplus.data.data), len(frequency_array)) +
                     "probably because padded with zeros up to the next power of two length." +
                     " Truncating lalsim array.")
        h_plus = hplus.data.data[:len(h_plus)]
        h_cross = hcross.data.data[:len(h_cross)]
    else:
        h_plus[:len(hplus.data.data)] = hplus.data.data
        h_cross[:len(hcross.data.data)] = hcross.data.data

    h_plus *= frequency_bounds
    h_cross *= frequency_bounds

    if wf_func == lalsim_SimInspiralFD:
        dt = 1 / hplus.deltaF + (hplus.epoch.gpsSeconds + hplus.epoch.gpsNanoSeconds * 1e-9)
        time_shift = np.exp(-1j * 2 * np.pi * dt * frequency_array[frequency_bounds])
        h_plus[frequency_bounds] *= time_shift
        h_cross[frequency_bounds] *= time_shift

    return dict(plus=h_plus, cross=h_cross)


def binary_black_hole_roq(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, **waveform_arguments):
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomPv2', reference_frequency=20.0)
    waveform_kwargs.update(waveform_arguments)
    return _base_roq_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_jl=phi_jl,
        phi_12=phi_12, lambda_1=0.0, lambda_2=0.0, **waveform_kwargs)


def binary_neutron_star_roq(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, lambda_1, lambda_2, theta_jn, phase,
        **waveform_arguments):
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomD_NRTidal', reference_frequency=20.0)
    waveform_kwargs.update(waveform_arguments)
    return _base_roq_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_jl=phi_jl,
        phi_12=phi_12, lambda_1=lambda_1, lambda_2=lambda_2, **waveform_kwargs)


def _base_roq_waveform(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, lambda_1, lambda_2, phi_jl, theta_jn, phase,
        **waveform_arguments):
    """
    See https://git.ligo.org/lscsoft/lalsuite/blob/master/lalsimulation/src/LALSimInspiral.c#L1460

    Parameters
    ==========
    frequency_array: np.array
        This input is ignored for the roq source model
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float

    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float

    theta_jn: float
        Orbital inclination
    phase: float
        The phase at coalescence

    Waveform arguments
    ===================
    Non-sampled extra data used in the source model calculation
    frequency_nodes_linear: np.array
    frequency_nodes_quadratic: np.array
    reference_frequency: float
    approximant: str

    Note: for the frequency_nodes_linear and frequency_nodes_quadratic arguments,
    if using data from https://git.ligo.org/lscsoft/ROQ_data, this should be
    loaded as `np.load(filename).T`.

    Returns
    =======
    waveform_polarizations: dict
        Dict containing plus and cross modes evaluated at the linear and
        quadratic frequency nodes.
    """
    from lal import CreateDict
    frequency_nodes_linear = waveform_arguments['frequency_nodes_linear']
    frequency_nodes_quadratic = waveform_arguments['frequency_nodes_quadratic']
    reference_frequency = waveform_arguments['reference_frequency']
    approximant = lalsim_GetApproximantFromString(
        waveform_arguments['waveform_approximant'])

    luminosity_distance = luminosity_distance * 1e6 * utils.parsec
    mass_1 = mass_1 * utils.solar_mass
    mass_2 = mass_2 * utils.solar_mass

    waveform_dictionary = CreateDict()
    lalsim_SimInspiralWaveformParamsInsertTidalLambda1(
        waveform_dictionary, lambda_1)
    lalsim_SimInspiralWaveformParamsInsertTidalLambda2(
        waveform_dictionary, lambda_2)

    iota, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y, spin_2z = bilby_to_lalsimulation_spins(
        theta_jn=theta_jn, phi_jl=phi_jl, tilt_1=tilt_1, tilt_2=tilt_2,
        phi_12=phi_12, a_1=a_1, a_2=a_2, mass_1=mass_1, mass_2=mass_2,
        reference_frequency=reference_frequency, phase=phase)

    h_linear_plus, h_linear_cross = lalsim_SimInspiralChooseFDWaveformSequence(
        phase, mass_1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
        spin_2z, reference_frequency, luminosity_distance, iota,
        waveform_dictionary, approximant, frequency_nodes_linear)

    waveform_dictionary = CreateDict()
    lalsim_SimInspiralWaveformParamsInsertTidalLambda1(
        waveform_dictionary, lambda_1)
    lalsim_SimInspiralWaveformParamsInsertTidalLambda2(
        waveform_dictionary, lambda_2)

    h_quadratic_plus, h_quadratic_cross = lalsim_SimInspiralChooseFDWaveformSequence(
        phase, mass_1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
        spin_2z, reference_frequency, luminosity_distance, iota,
        waveform_dictionary, approximant, frequency_nodes_quadratic)

    waveform_polarizations = dict()
    waveform_polarizations['linear'] = dict(
        plus=h_linear_plus.data.data, cross=h_linear_cross.data.data)
    waveform_polarizations['quadratic'] = dict(
        plus=h_quadratic_plus.data.data, cross=h_quadratic_cross.data.data)

    return waveform_polarizations


def binary_black_hole_frequency_sequence(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, **kwargs):
    """ A Binary Black Hole waveform model using lalsimulation. This generates
    a waveform only on specified frequency points. This is useful for
    likelihood requiring waveform values at a subset of all the frequency
    samples. For example, this is used for MBGravitationalWaveTransient.

    Parameters
    ==========
    frequency_array: array_like
        The input is ignored.
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the two component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total binary angular momentum and the
        orbital angular momentum
    theta_jn: float
        Angle between the total binary angular momentum and the line of sight
    phase: float
        The phase at coalescence
    kwargs: dict
        Required keyword arguments
        - frequencies:
            ndarray of frequencies at which waveforms are evaluated

        Optional keyword arguments
        - waveform_approximant
        - reference_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomPv2', reference_frequency=50.0,
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)
    return _base_waveform_frequency_sequence(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_jl=phi_jl,
        phi_12=phi_12, lambda_1=0.0, lambda_2=0.0, **waveform_kwargs)


def binary_neutron_star_frequency_sequence(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, lambda_1, lambda_2, theta_jn, phase,
        **kwargs):
    """ A Binary Neutron Star waveform model using lalsimulation. This generates
    a waveform only on specified frequency points. This is useful for
    likelihood requiring waveform values at a subset of all the frequency
    samples. For example, this is used for MBGravitationalWaveTransient.

    Parameters
    ==========
    frequency_array: array_like
        The input is ignored.
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the two component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total binary angular momentum and the
        orbital angular momentum
    lambda_1: float
        Dimensionless tidal deformability of mass_1
    lambda_2: float
        Dimensionless tidal deformability of mass_2
    theta_jn: float
        Angle between the total binary angular momentum and the line of sight
    phase: float
        The phase at coalescence
    kwargs: dict
        Required keyword arguments
        - frequencies:
            ndarray of frequencies at which waveforms are evaluated

        Optional keyword arguments
        - waveform_approximant
        - reference_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='IMRPhenomPv2_NRTidal', reference_frequency=50.0,
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)
    return _base_waveform_frequency_sequence(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_jl=phi_jl,
        phi_12=phi_12, lambda_1=lambda_1, lambda_2=lambda_2, **waveform_kwargs)


def _base_waveform_frequency_sequence(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, lambda_1, lambda_2, phi_jl, theta_jn, phase,
        **waveform_kwargs):
    """ Generate a cbc waveform model on specified frequency samples

    Parameters
    ----------
    frequency_array: np.array
        This input is ignored
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
    theta_jn: float
        Orbital inclination
    phase: float
        The phase at coalescence
    waveform_kwargs: dict
        Optional keyword arguments

    Returns
    -------
    waveform_polarizations: dict
        Dict containing plus and cross modes evaluated at the linear and
        quadratic frequency nodes.
    """
    from lal import CreateDict
    import lalsimulation as lalsim

    frequencies = waveform_kwargs['frequencies']
    reference_frequency = waveform_kwargs['reference_frequency']
    approximant = lalsim_GetApproximantFromString(waveform_kwargs['waveform_approximant'])
    catch_waveform_errors = waveform_kwargs['catch_waveform_errors']
    pn_spin_order = waveform_kwargs['pn_spin_order']
    pn_tidal_order = waveform_kwargs['pn_tidal_order']
    pn_phase_order = waveform_kwargs['pn_phase_order']
    pn_amplitude_order = waveform_kwargs['pn_amplitude_order']
    waveform_dictionary = waveform_kwargs.get(
        'lal_waveform_dictionary', CreateDict()
    )

    lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(
        waveform_dictionary, int(pn_spin_order))
    lalsim.SimInspiralWaveformParamsInsertPNTidalOrder(
        waveform_dictionary, int(pn_tidal_order))
    lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(
        waveform_dictionary, int(pn_phase_order))
    lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(
        waveform_dictionary, int(pn_amplitude_order))
    lalsim_SimInspiralWaveformParamsInsertTidalLambda1(
        waveform_dictionary, lambda_1)
    lalsim_SimInspiralWaveformParamsInsertTidalLambda2(
        waveform_dictionary, lambda_2)

    for key, value in waveform_kwargs.items():
        func = getattr(lalsim, "SimInspiralWaveformParamsInsert" + key, None)
        if func is not None:
            func(waveform_dictionary, value)

    if waveform_kwargs.get('numerical_relativity_file', None) is not None:
        lalsim.SimInspiralWaveformParamsInsertNumRelData(
            waveform_dictionary, waveform_kwargs['numerical_relativity_file'])

    if ('mode_array' in waveform_kwargs) and waveform_kwargs['mode_array'] is not None:
        mode_array = waveform_kwargs['mode_array']
        mode_array_lal = lalsim.SimInspiralCreateModeArray()
        for mode in mode_array:
            lalsim.SimInspiralModeArrayActivateMode(mode_array_lal, mode[0], mode[1])
        lalsim.SimInspiralWaveformParamsInsertModeArray(waveform_dictionary, mode_array_lal)

    luminosity_distance = luminosity_distance * 1e6 * utils.parsec
    mass_1 = mass_1 * utils.solar_mass
    mass_2 = mass_2 * utils.solar_mass

    iota, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y, spin_2z = bilby_to_lalsimulation_spins(
        theta_jn=theta_jn, phi_jl=phi_jl, tilt_1=tilt_1, tilt_2=tilt_2,
        phi_12=phi_12, a_1=a_1, a_2=a_2, mass_1=mass_1, mass_2=mass_2,
        reference_frequency=reference_frequency, phase=phase)

    try:
        h_plus, h_cross = lalsim_SimInspiralChooseFDWaveformSequence(
            phase, mass_1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
            spin_2z, reference_frequency, luminosity_distance, iota,
            waveform_dictionary, approximant, frequencies)
    except Exception as e:
        if not catch_waveform_errors:
            raise
        else:
            EDOM = (e.args[0] == 'Internal function call failed: Input domain error')
            if EDOM:
                failed_parameters = dict(mass_1=mass_1, mass_2=mass_2,
                                         spin_1=(spin_1x, spin_2y, spin_1z),
                                         spin_2=(spin_2x, spin_2y, spin_2z),
                                         luminosity_distance=luminosity_distance,
                                         iota=iota, phase=phase)
                logger.warning("Evaluating the waveform failed with error: {}\n".format(e) +
                               "The parameters were {}\n".format(failed_parameters) +
                               "Likelihood will be set to -inf.")
                return None
            else:
                raise

    return dict(plus=h_plus.data.data, cross=h_cross.data.data)


def sinegaussian(frequency_array, hrss, Q, frequency, **kwargs):
    tau = Q / (np.sqrt(2.0) * np.pi * frequency)
    temp = Q / (4.0 * np.sqrt(np.pi) * frequency)
    fm = frequency_array - frequency
    fp = frequency_array + frequency

    h_plus = ((hrss / np.sqrt(temp * (1 + np.exp(-Q**2)))) *
              ((np.sqrt(np.pi) * tau) / 2.0) *
              (np.exp(-fm**2 * np.pi**2 * tau**2) +
              np.exp(-fp**2 * np.pi**2 * tau**2)))

    h_cross = (-1j * (hrss / np.sqrt(temp * (1 - np.exp(-Q**2)))) *
               ((np.sqrt(np.pi) * tau) / 2.0) *
               (np.exp(-fm**2 * np.pi**2 * tau**2) -
               np.exp(-fp**2 * np.pi**2 * tau**2)))

    return{'plus': h_plus, 'cross': h_cross}


def supernova(
        frequency_array, realPCs, imagPCs, file_path, luminosity_distance, **kwargs):
    """ A supernova NR simulation for injections """

    realhplus, imaghplus, realhcross, imaghcross = np.loadtxt(
        file_path, usecols=(0, 1, 2, 3), unpack=True)

    # waveform in file at 10kpc
    scaling = 1e-3 * (10.0 / luminosity_distance)

    h_plus = scaling * (realhplus + 1.0j * imaghplus)
    h_cross = scaling * (realhcross + 1.0j * imaghcross)
    return {'plus': h_plus, 'cross': h_cross}


def supernova_pca_model(
        frequency_array, pc_coeff1, pc_coeff2, pc_coeff3, pc_coeff4, pc_coeff5,
        luminosity_distance, **kwargs):
    """ Supernova signal model """

    realPCs = kwargs['realPCs']
    imagPCs = kwargs['imagPCs']

    pc1 = realPCs[:, 0] + 1.0j * imagPCs[:, 0]
    pc2 = realPCs[:, 1] + 1.0j * imagPCs[:, 1]
    pc3 = realPCs[:, 2] + 1.0j * imagPCs[:, 2]
    pc4 = realPCs[:, 3] + 1.0j * imagPCs[:, 3]
    pc5 = realPCs[:, 4] + 1.0j * imagPCs[:, 5]

    # file at 10kpc
    scaling = 1e-23 * (10.0 / luminosity_distance)

    h_plus = scaling * (pc_coeff1 * pc1 + pc_coeff2 * pc2 + pc_coeff3 * pc3 +
                        pc_coeff4 * pc4 + pc_coeff5 * pc5)
    h_cross = scaling * (pc_coeff1 * pc1 + pc_coeff2 * pc2 + pc_coeff3 * pc3 +
                         pc_coeff4 * pc4 + pc_coeff5 * pc5)

    return {'plus': h_plus, 'cross': h_cross}


precession_only = {
    "tilt_1", "tilt_2", "phi_12", "phi_jl", "chi_1_in_plane", "chi_2_in_plane",
}

spin = {
    "a_1", "a_2", "tilt_1", "tilt_2", "phi_12", "phi_jl", "chi_1", "chi_2",
    "chi_1_in_plane", "chi_2_in_plane",
}
mass = {
    "chirp_mass", "mass_ratio", "total_mass", "mass_1", "mass_2",
    "symmetric_mass_ratio",
}
primary_spin_and_q = {
    "a_1", "chi_1", "mass_ratio"
}
tidal = {
    "lambda_1", "lambda_2", "lambda_tilde", "delta_lambda_tilde"
}
phase = {
    "phase", "delta_phase",
}
extrinsic = {
    "azimuth", "zenith", "luminosity_distance", "psi", "theta_jn",
    "cos_theta_jn", "geocent_time", "time_jitter", "ra", "dec",
    "H1_time", "L1_time", "V1_time",
}

PARAMETER_SETS = dict(
    spin=spin, mass=mass, phase=phase, extrinsic=extrinsic,
    tidal=tidal, primary_spin_and_q=primary_spin_and_q,
    intrinsic=spin.union(mass).union(phase).union(tidal),
    precession_only=precession_only,
)

"""
Description: bilby source module for gwsurrogate NRHybSur3dq8 waveform model
Note: this should be added to the PATH-TO-BILBY/bilby/gw/source.py file

Modified version from Tousif Islam,July 10, 2020
Carl-Johan Haster, 27 October, 2020

"""

import gwsurrogate
from scipy.interpolate import InterpolatedUnivariateSpline #as spline
#import scipy.integrate as integrate
from gwtools import gwutils as gwu
from ..core.utils import nfft

from lal import MTSUN_SI as lal_MTSUN_SI

sur = gwsurrogate.LoadSurrogate('NRHybSur3dq8')
#trunc_sur = gwsurrogate.LoadSurrogate('../TruncatedSurrogate/NRHybSur3dq8.h5')
trunc_sur = gwsurrogate.LoadSurrogate('/home/carl-johan.haster/Repos/Waveform_marginalisation/TruncatedSurrogate/NRHybSur3dq8.h5')

def gwsurrogate_marginalise_binary_black_hole(frequency_array,
   total_mass, mass_ratio, chi_1, chi_2,
   luminosity_distance, theta_jn, phase, **kwargs):

    """ A Binary Black Hole waveform model using gwsurrogate

    Parameters
    ----------
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    total_mass: float
        The total mass of the binary in solar masses
    mass_ratio: float
        The binary mass ratio, note mass_ratio < 1 in the input,
        but q > 1 in the function below
    luminosity_distance: float
        The luminosity distance in megaparsec
    chi_1: float
        Dimensionless primary spin magnitude, only valid for
        spins aligned with orbital angular momentum
    chi_2: float
        Dimensionless secondaryspin magnitude, only valid for
        spins aligned with orbital angular momentum
    theta_jn: float
        Angle between the total binary angular momentum and the line of sight
        For alignedSpin binaries, same as inclination
    phase: float
        The phase at coalescence
    kwargs: dict
        Optional keyword arguments
        Supported arguments:
            waveform_approximant
            reference_frequency
            minimum_frequency
            maximum_frequency
            catch_waveform_errors
            PtsPerOrbit
            activate_marginalisation

    Returns
    -------
    dict: A dictionary with the plus and cross polarisation strain modes
    """

    waveform_kwargs = dict(
        waveform_approximant='NRHybSur3dq8',
        reference_frequency=20.0,
        minimum_frequency=20.0,
        maximum_frequency=2048.,
        catch_waveform_errors=False,
        PtsPerOrbit=5,
        m_max=5,
        activate_marginalisation=False)
    waveform_kwargs.update(kwargs)

    f_ref = waveform_kwargs['reference_frequency']
    f_min = waveform_kwargs['minimum_frequency']
    f_max = waveform_kwargs['maximum_frequency']
    catch_waveform_errors = waveform_kwargs['catch_waveform_errors']
    PtsPerOrbit = waveform_kwargs['PtsPerOrbit']
    activate_marginalisation = waveform_kwargs['activate_marginalisation']
    m_max = waveform_kwargs['m_max']

    df = frequency_array[1] - frequency_array[0]
    sampling_frequency = 2.*f_max
    dt = 1./sampling_frequency

    frequency_bounds = np.logical_and(np.greater_equal(frequency_array, f_min),
       np.less_equal(frequency_array, f_max))

    # Again, q > 1
    q = 1./mass_ratio

    # The WF-generator takes a spin vector, but only the z-component is populated
    spin1 = [0., 0., chi_1]
    spin2 = [0., 0., chi_2]

    # Get surrogate and truncated surrogate in coorbital frame
    # Note that unless activate_marginalisation=True, hCoOrbdict_trunc
    # is an empty dictionary
    times, hCoOrbdict, hCoOrbdict_trunc = evaluate_surrogates(
                             total_mass, q, spin1, spin2, luminosity_distance,
                             f_ref, f_min, dt, activate_marginalisation)

    # use orbital phase to get sparse times such that there are 5 points per orbit
    # FIXME: This will oversample the post ringdown, so throw those away
    phi_orb = hCoOrbdict['phi_l2m2']/2
    t_sparse = get_uniform_in_orbits_times(times, phi_orb, PTS_PER_ORBIT=PtsPerOrbit)

    # get indices of times that are closest to t_sparse
    # NOTE: We could downsample by interpolating using splines. But if the
    # data becomes very long, this becomes expensive. So, instead we just
    # find the incdices of times that are closest to t_sparse, which is just as
    # valid a way to downsample, but very cheap!
    idx_sparse = np.searchsorted(times, t_sparse)

    if activate_marginalisation:

       # We want times sampled PtsPerOrbit times per orbit, for each m-mode
       idx_sparse_list = find_sparse_time_indecies(times, phi_orb,
          m_max=m_max, PTS_PER_ORBIT=PtsPerOrbit)

       # Get marginalized coorbital frame data
       #hCoOrbdict_marg = {}
       hCoOrbdict_margSparse = {}
       for key in hCoOrbdict.keys():
           data = hCoOrbdict[key]
           data_trunc = hCoOrbdict_trunc[key]
           m = int(key.split('m')[-1])

           #hCoOrbdict_marg[key] = add_noise_to_data_piece(data, data_trunc, idx_sparse)
           hCoOrbdict_margSparse[key] = add_sparse_noise_to_data_piece(times,
              data, data_trunc, idx_sparse_list[m])

       # Set initial phase to zero after marginalization
       #hCoOrbdict_marg['phi_l2m2'] -= hCoOrbdict_marg['phi_l2m2'][0]
       hCoOrbdict_margSparse['phi_l2m2'] -= hCoOrbdict_margSparse['phi_l2m2'][0]

       # NOTE: see the `else` statement below for when WF marginalisation is not
       # activated.
       # The rest of this function below is the same for both cases, just acting on
       # different WF data

       # Transform to inertial frame
       #hdict_marg = transform_to_inertial_frame(hCoOrbdict_marg)
       hdict_margSparse = transform_to_inertial_frame(hCoOrbdict_margSparse)

       # Align initial orbital phase for plots
       #hdict_marg = align_orbital_phase(hdict_marg)
       hdict_margSparse = align_orbital_phase(hdict_margSparse)

       # project modes following LAL convention
       #h_marg = sur._mode_sum(hdict_marg, incl, np.pi/2 - phi_ref, fake_neg_modes=True)
       h = sur._mode_sum(hdict_margSparse, theta_jn, np.pi/2 - phase, fake_neg_modes=True)

    else:

       # Transform to inertial frame
       hdict = transform_to_inertial_frame(hCoOrbdict)

       # Align initial orbital phase for plots
       hdict = align_orbital_phase(hdict)

       # project modes following LAL convention
       h = sur._mode_sum(hdict, theta_jn, np.pi/2 - phase, fake_neg_modes=True)

    beginning_time = times[0]
    twoOrbitsAfter_beginning_time = times[idx_sparse[2*PtsPerOrbit]]
    SeventyFiveM = 75.*lal_MTSUN_SI*total_mass
    HundredM = 100.*lal_MTSUN_SI*total_mass

    h_Plancked = gwu.windowWaveform(times, h,
       beginning_time, twoOrbitsAfter_beginning_time,
       SeventyFiveM, HundredM, windowType="planck")

    #details before zero padding
    #difference in length between generated gwsurrogate waveform and target bilby-compatible waveform
    time_diff = (2*(len(frequency_array)-1)) - len(times)

    # If the difference is zero, no need to worry; just go ahead.
    # Otherwise the following if loops would take care of it
    # Case 1: generated waveform is shorter in length than data
    if time_diff > 0:
       #zero pad in the inspiral
       h_Plancked = np.pad(h_Plancked, (time_diff, 0), 'constant')
       # NOTE: nominally there are Tukey windows applied here
       # I'm skipping that for now...

    elif time_diff < 0:
       #remove from inspiral
       h_Plancked = h_Plancked[-time_diff:]

       # need to window new beginning of WF
       cut_beginning_time = times[-time_diff:][0]
       cut_t_sparse = t_sparse[np.greater_equal(t_sparse, cut_beginning_time)]
       cut_idx_sparse = np.searchsorted(times[-time_diff:], cut_t_sparse)
       cut_twoOrbitsAfter_beginning_time = times[-time_diff:][cut_idx_sparse[2*PtsPerOrbit]]

       h_Plancked *= gwu.window(times[-time_diff:],
          cut_beginning_time, cut_twoOrbitsAfter_beginning_time,  windowType='planck')

    #h_plus_TD = h_Plancked.real
    #h_cross_TD = -1*h_Plancked.imag

    h_plus, h_plus_freq = nfft(h_Plancked.real, sampling_frequency=sampling_frequency)
    h_cross, h_cross_freq = nfft(-1*h_Plancked.imag, sampling_frequency=sampling_frequency)

    h_plus *= frequency_bounds
    h_cross *= frequency_bounds

    return {'plus': h_plus, 'cross': h_cross}

# --------------------------------------------------------------------------
def evaluate_surrogates(total_mass, q, spin1, spin2, luminosity_distance,
   f_ref, f_min, dt, activate_marginalisation):

    # evaluate waveform modes for full surrogate and transform to coorbital frame
    t, hdict, _ = sur(q=q, chiA0=spin1, chiB0=spin2, dt=dt, f_low=f_min, f_ref=f_ref, \
        dist_mpc=luminosity_distance, M=total_mass, units='mks')
    hCoOrbdict = transform_to_coorbital_frame(t, hdict)

    # Set initial phase to zero
    hCoOrbdict['phi_l2m2'] -= hCoOrbdict['phi_l2m2'][0]

    if activate_marginalisation:

       #print('inside activate_marginalisation')

       # similarly, evaluate truncated surrogate and transform to coorbital frame
       t_trunc, hdict_trunc, _ = trunc_sur(q=q, chiA0=spin1, chiB0=spin2, dt=dt, f_low=f_min, \
           f_ref=f_ref, dist_mpc=luminosity_distance, M=total_mass, units='mks')
       hCoOrbdict_trunc = transform_to_coorbital_frame(t_trunc, hdict_trunc)

       # interpolate onto a common time array
       # extra caution to avoid extrapolation
       times = np.arange(max(t[1], t_trunc[1]), min(t[-2], t[-2]), dt)
       hCoOrbdict = interpolate_coorbital_frame_data(t, hCoOrbdict, times)
       hCoOrbdict_trunc = interpolate_coorbital_frame_data(t_trunc, hCoOrbdict_trunc, times)

       # Set initial phase to zero
       hCoOrbdict_trunc['phi_l2m2'] -= hCoOrbdict_trunc['phi_l2m2'][0]

    else:
       #print('not inside activate_marginalisation')
       times = t
       hCoOrbdict_trunc = {}

    return times, hCoOrbdict, hCoOrbdict_trunc

# --------------------------------------------------------------------------
def spline_interp(newX, oldX, oldY):
    if len(oldY) != len(oldX):
        raise Exception('Lengths dont match.')

    if np.min(newX) < np.min(oldX) or np.max(newX) > np.max(oldX):
        print (newX[0], oldX[0], newX[-1], oldX[-1])
        raise Exception('Trying to extrapolate, disabled for now')

    newY = InterpolatedUnivariateSpline(oldX, oldY, ext=1)(newX)      # returns 0 when extrapolating
    return newY

# --------------------------------------------------------------------------
def spline_interp_ext0(newX, oldX, oldY):
    if len(oldY) != len(oldX):
        raise Exception('Lengths dont match.')

    newY = InterpolatedUnivariateSpline(oldX, oldY, ext='zeros')(newX)      # returns 0 when extrapolating
    return newY

# --------------------------------------------------------------------------
def interpolate_coorbital_frame_data(t_sparse, hCoOrb_sparse, t_new):
    hCoOrb = {}
    for key in hCoOrb_sparse.keys():
        data = hCoOrb_sparse[key]
        if np.iscomplexobj(data):
            hCoOrb[key] = \
                spline_interp(t_new, t_sparse, np.real(data)) \
                +1.j*spline_interp(t_new, t_sparse, np.imag(data))
        else:
            hCoOrb[key] = spline_interp(t_new, t_sparse, data)
    return hCoOrb

#-----------------------------------------------------------------------------
def transform_to_coorbital_frame(t, hdict):
    """ Transforms from inertial frame to coorbital frame.

    Returns coorbital frame waveform modes as a dictionary, hCoOrb_dict.
    Usage:
        Amp_l2m2 = hCoOrb_dict['Amp_l2m2']
        phi_l2m2 = hCoOrb_dict['phi_l2m2']
        hCoOrb_l3m1 = hCoOrb_dict['hCoOrb_l3m1']    # similar for all other modes
    """

    h_22 = hdict[(2,2)]
    Amp_22 = np.abs(h_22)
    phi_22 = -np.unwrap(np.angle(h_22))

    # save coorbital frame strain as a dict
    hCoOrbdict = {}
    hCoOrbdict['Amp_l2m2'] = Amp_22
    hCoOrbdict['phi_l2m2'] = phi_22


    #plt.figure(figsize=(12,8))
    for mode in hdict.keys():

        l,m = mode
        if not (l==2 and m==2):     # for (2,2) mode we save Amp,phase instead
            hlm = hdict[(l,m)]
            hCoOrbdict['hCoOrb_l%dm%d'%(l,m)] = hlm*np.exp(1j*m*phi_22/2.)

    # Set initial phase to zero
    hCoOrbdict['phi_l2m2'] -= hCoOrbdict['phi_l2m2'][0]

    if np.max(np.abs((h_22*np.exp(1j*2*phi_22/2.)).imag)) > 1e-6:
        raise Exception('Imag part of 2,2 mode in coorbital frame should be 0 for nonprecessing cases.')

    return hCoOrbdict

# --------------------------------------------------------------------------
def transform_to_inertial_frame(hCoOrbdict):
    """ Transforms from coorbital frame to inertial frame.
    """
    hdict = {}
    phi22 = hCoOrbdict['phi_l2m2']
    hdict[(2,2)] = hCoOrbdict['Amp_l2m2'] * np.exp(-1j*phi22)
    for key in hCoOrbdict.keys():
        if key[:2] == "hC":
            l = int(key.split('_l')[-1].split('m')[0])
            m = int(key.split('m')[-1])
            hdict[(l,m)] = hCoOrbdict[key]*np.exp(-1j*m*phi22/2.)

    return hdict

# --------------------------------------------------------------------------
def align_orbital_phase(hdict):
    init_orbphase = -np.angle(hdict[(2,2)][0])/2
    for mode in hdict.keys():
        l,m = mode
        hdict[(l,m)] = hdict[(l,m)] * np.exp(1j*m*init_orbphase)
    return hdict

# --------------------------------------------------------------------------
def get_uniform_in_orbits_times(t, phi_orb, PTS_PER_ORBIT=5):
    """
    returns sparse time array such that there are PTS_PER_ORBIT points
    in each orbit.
    """
    # get number of orbits
    n_orbits = int(abs((phi_orb[-1] - phi_orb[0])/(2*np.pi)))

    # get sparse times such that there are PTS_PER_ORBIT points in each orbit
    n_pts = int(n_orbits*PTS_PER_ORBIT)
    phi_orb_sparse = np.linspace(phi_orb[0], phi_orb[-1], n_pts)
    t_sparse = np.interp(phi_orb_sparse, phi_orb, t)

    return t_sparse

# --------------------------------------------------------------------------
def find_sparse_time_indecies(t, phi_orb, m_max=5, PTS_PER_ORBIT=5):
    """
    returns sparse time index array for each m-mode such that there are PTS_PER_ORBIT points
    in each orbit.
    """
    idx_sparse_list = [None]*(m_max+1)

    for m in range(1, m_max+1):
        n_orbits = int(abs(m*(phi_orb[-1] - phi_orb[0])/(2*np.pi)))
        n_pts = int(n_orbits*PTS_PER_ORBIT)
        phi_orb_sparse = np.linspace(phi_orb[0], phi_orb[-1], n_pts)
        t_sparse = np.interp(phi_orb_sparse, phi_orb, t)

        # get indices of times that are closest to t_sparse
        # NOTE: We could downsample by interpolating using splines. But if the
        # data becomes very long, this becomes expensive. So, instead we just
        # find the incdices of times that are closest to t_sparse, which is just as
        # valid a way to downsample, but very cheap!
        idx_sparse_withPossibleDuplicates_andLastIndex = np.searchsorted(t, t_sparse)
        idx_sparse_withPossibleDuplicates = \
        idx_sparse_withPossibleDuplicates_andLastIndex[np.less(idx_sparse_withPossibleDuplicates_andLastIndex, len(t))]
        # we also don't include the last index, since that breaks the array picks
        # ie one can't pick out index 1023 from an array of length 1023

        # The indecies sometimes have duplicate values at late times (ie past the ringdown)
        # The line below then removes any duplicate indecies from the array
        idx_sparse_complete = np.array(list(dict.fromkeys(idx_sparse_withPossibleDuplicates.tolist())))

        # sometimes the last index was not monotonically increasing
        # so I remove the last 3 just to be sure :)
        idx_sparse = idx_sparse_complete[:(len(idx_sparse_complete)-3)]

        idx_sparse_list[m] = idx_sparse
        if m == 1:
            idx_sparse_list[0] = idx_sparse

    return idx_sparse_list

# --------------------------------------------------------------------------
def add_sparse_noise_to_data_piece(times, data, data_trunc, idx_sparse):

    sparse_data_diff = data[idx_sparse] - data_trunc[idx_sparse]

    sparse_data_scatter_real = np.random.normal(0, np.abs(sparse_data_diff.real))#*np.sign(sparse_data_diff.real)
    try:
       interp_sparse_data_scatter_real = spline_interp_ext0(times, times[idx_sparse], \
                                                         np.abs(sparse_data_scatter_real)*np.sign(sparse_data_diff.real))

    except:
       print(times.shape, times)
       print(times[idx_sparse].shape, times[idx_sparse])
       output_testDir = '/home/haster/projects/WFmarg/Runs/outdir_TestInj_305_60Msun_LowSNR_NRSurMarg/log_data_analysis'
       np.savetxt(output_testDir+'/times.dat', times)
       np.savetxt(output_testDir+'/idx_sparse.dat', idx_sparse)
       np.savetxt(output_testDir+'/times_idx_sparse.dat', times[idx_sparse])

    data_marg = data.real + interp_sparse_data_scatter_real

    if np.iscomplexobj(sparse_data_diff):
        sparse_data_scatter_imag = np.random.normal(0, np.abs(sparse_data_diff.imag))
        interp_sparse_data_scatter_imag = spline_interp_ext0(times, times[idx_sparse], \
                                                         np.abs(sparse_data_scatter_imag)*np.sign(sparse_data_diff.imag))

        data_marg = data_marg + 1j*(data.imag + interp_sparse_data_scatter_imag)

    return data_marg
