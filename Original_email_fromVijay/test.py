#############################################################################
##
##      Filename: test.py
##
##      Author: Vijay Varma
##
##      Created: 20-09-2019
##
##      Description:
##
#############################################################################

import numpy as np
import matplotlib.pyplot as P
import gwsurrogate

# Load the surrogate, this should only be done once
sur = gwsurrogate.LoadSurrogate('NRHybSur3dq8')
trunc_sur = gwsurrogate.LoadSurrogate('TruncatedSurrogate/NRHybSur3dq8.h5')

q = 7
chiA = [0, 0, 0.5]
chiB = [0, 0, -0.7]
M = 60             # Total masss in solar masses
dist_mpc = 100     # distance in megaparsecs
dt = 1./4096       # step size in seconds
f_low = 20         # initial frequency in Hz
inclination = np.pi/4
phi_ref = np.pi/5

# For more options such as using only a subset of modes, see
# https://data.black-holes.org/surrogates/NRHybSur3dq8.html

# Returns h_+ -i h_x
t, h, _ = sur(q, chiA, chiB, dt=dt, f_low=f_low, M=M, dist_mpc=dist_mpc,
           inclination=inclination, phi_ref=phi_ref, units='mks')
t_trunc, h_trunc, _ = trunc_sur(q, chiA, chiB, dt=dt, f_low=f_low, M=M, \
    dist_mpc=dist_mpc, inclination=inclination, phi_ref=phi_ref, units='mks')

P.plot(t, h.real, label='full surrogate')
P.plot(t_trunc, h_trunc.real, label='truncated surrogate')
P.plot(t, h.real-h_trunc.real, label='diff')
P.legend(loc='best')
P.ylabel('$h_{+}$ $(\iota, \phi_{ref})$', fontsize=18)
P.xlabel('t [s]', fontsize=18)
P.show()
